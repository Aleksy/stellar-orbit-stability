public class BlackHoleAngularDiameter {
    public static double calculate(double blackHoleMass, double distanceFromObserver) {
        double schwarzschildRadius = SchwarzschildRadius.calculate(blackHoleMass);
        return 2 * Math.atan(schwarzschildRadius / (2 * distanceFromObserver));
    }

    /**
     * Calculates angular diameter given in radians to the "moon fraction" unit. Calculated result "1" means that
     * object is exactly as big as the moon on the sky.
     * @param radians to be calculated
     * @return angular diameter in "moon fractions"
     */
    public static double toMoonFractions(double radians) {
        final double moonAngularSizeInRadians = 0.00873;
        return radians / moonAngularSizeInRadians;
    }

}
