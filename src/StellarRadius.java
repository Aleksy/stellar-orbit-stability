public class StellarRadius {
    public static double calculate(double massDwarf) {
        final double RADIUS_SOLAR = 6.957e8; // Solar radius in meters
        final double MASS_SOLAR = 1.989e30; // Solar mass in kg

        // Estimate the stellar radius using the mass-radius relation for main-sequence stars
        // The exponent 0.8 is an empirical value derived from observational data
        return RADIUS_SOLAR * Math.pow(massDwarf / MASS_SOLAR, 0.8);
    }
}
