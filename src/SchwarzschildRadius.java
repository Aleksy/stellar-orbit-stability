public class SchwarzschildRadius {
    public static double calculate(double mass) {
        return (2 * PhysicalConstants.GRAVITATIONAL_CONSTANT * mass) / Math.pow(PhysicalConstants.LIGHT_SPEED, 2);
    }
}
