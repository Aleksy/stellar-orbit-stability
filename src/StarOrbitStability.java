public class StarOrbitStability {
    // Weights for the stability coefficients
    private static final double W1 = 0.25;
    private static final double W2 = 0.25;
    private static final double W3 = 0.25;
    private static final double W4 = 0.25;

    public static double calculate(double radiusOrbit, double massBlackHole, double massDwarf) {
        double s1 = calculateS1(radiusOrbit, massBlackHole);
        double s2 = calculateS2(radiusOrbit, massBlackHole);
        double s3 = calculateS3(radiusOrbit, massBlackHole, massDwarf);
        double s4 = calculateS4(radiusOrbit, massBlackHole, massDwarf);
        return W1 * s1 + W2 * s2 + W3 * s3 + W4 * s4;
    }

    // Stability coefficient based on the distance from the event horizon
    private static double calculateS1(double radiusOrbit, double massBlackHole) {
        double schwarzschildRadius = SchwarzschildRadius.calculate(massBlackHole);
        return (radiusOrbit - schwarzschildRadius) / radiusOrbit;
    }

    // Stability coefficient based on orbital velocity compared to the speed of light
    private static double calculateS2(double radiusOrbit, double massBlackHole) {
        double velocity = Math.sqrt(PhysicalConstants.GRAVITATIONAL_CONSTANT * massBlackHole / radiusOrbit);
        return 1 - velocity / PhysicalConstants.LIGHT_SPEED;
    }

    // Stability coefficient based on the Roche limit
    private static double calculateS3(double radiusOrbit, double massBlackHole, double massDwarf) {
        double rocheLimit = StellarRadius.calculate(massDwarf) * Math.pow(2 * massBlackHole / massDwarf, 1.0 / 3.0);
        return (radiusOrbit - rocheLimit) / radiusOrbit;
    }

    // Stability coefficient based on gravitational force
    private static double calculateS4(double radiusOrbit, double massBlackHole, double massDwarf) {
        // Calculate the gravitational force at the orbital radius
        double gravitationalForce = PhysicalConstants.GRAVITATIONAL_CONSTANT * massBlackHole * massDwarf / (radiusOrbit * radiusOrbit);
        // Calculate the Roche limit
        double rocheLimit = RocheLimit.calculate(massBlackHole, massDwarf, StellarRadius.calculate(massDwarf));
        // Calculate the gravitational force at the Roche limit
        double rocheForce = PhysicalConstants.GRAVITATIONAL_CONSTANT * massBlackHole * massDwarf / (rocheLimit * rocheLimit);
        // Calculate the stability coefficient
        return (rocheForce - gravitationalForce) / rocheForce;
    }
}
