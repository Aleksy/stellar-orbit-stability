public enum BlackHoleClass {
    NOT_A_BLACK_HOLE("not a black hole!"),
    STELLAR_BLACK_HOLE("the Stellar Black Hole"),
    INTERMEDIATE_BLACK_HOLE("the Intermediate Black Hole"),
    SUPERMASSIVE_BLACK_HOLE("the Supermassive Black Hole");

    private String name;

    private BlackHoleClass(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
