public class RocheLimit {
    public static double calculate(double blackHoleMass, double starMass, double starRadius) {
        return starRadius * Math.pow(2 * blackHoleMass / starMass, 1.0 / 3.0);
    }
}
