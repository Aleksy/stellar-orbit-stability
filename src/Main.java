import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        StellarStabilityApp.start(retrieveArgs(args));
    }

    /*
    ARGS
    -blackHoleMass (unit: mass of sun) 10
    -starMass (unit: mass of sun) 0.7
    -starToBlackHoleDistance (unit: AU) 5
    -planetToStarDistance (unit: AU) 0.45
    -blackHoleAngularDiameter (unit: radians) 0.00873
     */
    private static Map<String, String> retrieveArgs(String[] args) {
        Map<String, String> arguments = new HashMap<>();
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                if (i + 1 < args.length && !args[i + 1].startsWith("-")) {
                    arguments.put(args[i], args[i + 1]);
                    i++;
                } else {
                    arguments.put(args[i], null);
                }
            }
        }
        return arguments;
    }
}