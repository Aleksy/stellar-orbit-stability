public class TechnicalConstants {
    // unit: mass of sun
    public static final String BLACK_HOLE_MASS_ARG = "-blackHoleMass";
    // unit: mass of sun
    public static final String STAR_MASS_ARG = "-starMass";
    // unit: AU
    public static final String STAR_TO_BLACK_HOLE_DISTANCE_ARG = "-starToBlackHoleDistance";
    // unit: AU
    public static final String PLANET_TO_STAR_DISTANCE_ARG = "-planetToStarDistance";
    public static final String MODE_ARG = "-mode";
}
