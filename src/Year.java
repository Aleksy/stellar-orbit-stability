public class Year {

    public static double calculate(double starMassKilograms, double planetToStarDistanceMeters) {
        double orbitalSpeed = Math.sqrt((PhysicalConstants.GRAVITATIONAL_CONSTANT * starMassKilograms) / planetToStarDistanceMeters);
        double orbitDistance = 2 * Math.PI * planetToStarDistanceMeters;
        double time = orbitDistance / orbitalSpeed;
        return time / 60 / 60 / 24;
    }
}
