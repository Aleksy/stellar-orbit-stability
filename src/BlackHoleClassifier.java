public class BlackHoleClassifier {
    public static BlackHoleClass classifyBlackHole(double blackHoleMassInSolarMasses) {
        if (blackHoleMassInSolarMasses < 3) {
            return BlackHoleClass.NOT_A_BLACK_HOLE;
        } else if (blackHoleMassInSolarMasses <= 100) {
            return BlackHoleClass.STELLAR_BLACK_HOLE;
        } else if (blackHoleMassInSolarMasses <= 1_000_000) {
            return BlackHoleClass.INTERMEDIATE_BLACK_HOLE;
        } else {
            return BlackHoleClass.SUPERMASSIVE_BLACK_HOLE;
        }
    }
}