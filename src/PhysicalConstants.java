public class PhysicalConstants {
    public static final double LIGHT_SPEED = 299792458.0;
    public static final double GRAVITATIONAL_CONSTANT = 6.67430e-11;
    public static final double SOLAR_MASS = 1.989e30;
    public static final double AU = 1.496e11;
}
