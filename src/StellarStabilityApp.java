import java.util.Map;

public class StellarStabilityApp {

    private static final String COMPACT = "compact";

    private final Map<String, String> arguments;

    private boolean compactMode;

    public StellarStabilityApp(Map<String, String> args) {
        this.arguments = args;
    }

    private void run() {
        Double blackHoleMass = retrieve(TechnicalConstants.BLACK_HOLE_MASS_ARG);
        Double starMass = retrieve(TechnicalConstants.STAR_MASS_ARG);
        Double starToBlackHoleDistance = retrieve(TechnicalConstants.STAR_TO_BLACK_HOLE_DISTANCE_ARG);
        Double planetToStarDistance = retrieve(TechnicalConstants.PLANET_TO_STAR_DISTANCE_ARG);
        String mode = retrieveString(TechnicalConstants.MODE_ARG);
        compactMode = COMPACT.equals(mode);

        double blackHoleMassKilograms = blackHoleMass * PhysicalConstants.SOLAR_MASS;
        double starMassKilograms = starMass * PhysicalConstants.SOLAR_MASS;
        double starToBlackHoleDistanceMeters = starToBlackHoleDistance * PhysicalConstants.AU;
        double planetToStarDistanceMeters = planetToStarDistance * PhysicalConstants.AU;
        double orbitRadiusWithThePlanetTakenIntoAccountMeters = starToBlackHoleDistanceMeters - planetToStarDistanceMeters;

        double starOrbitStability = StarOrbitStability.calculate(orbitRadiusWithThePlanetTakenIntoAccountMeters, blackHoleMassKilograms, starMassKilograms);

        double calculatedBlackHoleAngularDiameter = BlackHoleAngularDiameter.calculate(blackHoleMassKilograms, starToBlackHoleDistanceMeters + planetToStarDistanceMeters);
        double calculatedBlackHoleAngularDiameterInMoonFractions = BlackHoleAngularDiameter.toMoonFractions(calculatedBlackHoleAngularDiameter);
        double calculatedBlackHoleAngularDiameterNearest = BlackHoleAngularDiameter.calculate(blackHoleMassKilograms, starToBlackHoleDistanceMeters - planetToStarDistanceMeters);
        double calculatedBlackHoleAngularDiameterNearestInMoonFractions = BlackHoleAngularDiameter.toMoonFractions(calculatedBlackHoleAngularDiameterNearest);

        double planetYearInDays = Year.calculate(starMassKilograms, planetToStarDistanceMeters);
        double starYearInDays = Year.calculate(blackHoleMassKilograms, starToBlackHoleDistanceMeters);
        BlackHoleClass blackHoleClass = BlackHoleClassifier.classifyBlackHole(blackHoleMass);

        println("STELLAR STABILITY APP", "");
        println("Given params:", "");
        println("Black hole mass: " + blackHoleMass);
        println("Star mass: " + starMass);
        println("Distance from Black Hole to Star: " + starToBlackHoleDistance);
        println("Distance from Star to Planet: " + planetToStarDistance);
        println();

        println("ORBIT STABILITY");
        // black hole classification
        println("With the mass of " + blackHoleMass + " solar masses, the Black Hole seems to be the " + blackHoleClass,
                "Black hole class: " + blackHoleClass);
        if (orbitRadiusWithThePlanetTakenIntoAccountMeters < 0) {
            println("WARNING: Star too close. Planet fell into the Black Hole");
        }
        println("With this parameters the stability factor is " + round(starOrbitStability, 6),
                "Stability factor: " + round(starOrbitStability, 6));
        println();
        println("BLACK HOLE PARAMETERS");
        println("Schwarzschild's radius: " + round(SchwarzschildRadius.calculate(blackHoleMassKilograms) / 1000, 3) + " kilometers.");
        println();


        // angular diameter from mass and distance
        println("VISIBILITY ON THE SKY (THE FARTHEST PERSPECTIVE = STAR DISTANCE + PLANET DISTANCE)",
                "VISIBILITY ON THE SKY (THE FARTHEST)");
        printAngularDiameters(calculatedBlackHoleAngularDiameterInMoonFractions);

        println("\nVISIBILITY ON THE SKY (THE NEAREST PERSPECTIVE = STAR DISTANCE - PLANET DISTANCE)",
                "\nVISIBILITY ON THE SKY (THE NEAREST)");
        printAngularDiameters(calculatedBlackHoleAngularDiameterNearestInMoonFractions);
        println("\nORBIT PARAMETERS");
        println("Planet's year: " + round(planetYearInDays, 2) + " Earth's days (" + round(planetYearInDays / 365, 2) + " Earth's years)");
        println("Star's year: " + round(starYearInDays, 2) + " Earth's days (" + round(starYearInDays / 365, 2) + " Earth's years)");
    }

    private void printAngularDiameters(double calculatedBlackHoleAngularDiameterInMoonFractions) {
        println("Angular diameter calculated from given Black Hole mass and distance from Star to Black Hole is "
                + calculatedBlackHoleAngularDiameterInMoonFractions + " of moon's angular diameter",
                "Angular diameter of the Black Hole: "
                        + calculatedBlackHoleAngularDiameterInMoonFractions + " of moon's size");
        printAccretionDiskDiameters(calculatedBlackHoleAngularDiameterInMoonFractions, 50);
        printAccretionDiskDiameters(calculatedBlackHoleAngularDiameterInMoonFractions, 100);
        printAccretionDiskDiameters(calculatedBlackHoleAngularDiameterInMoonFractions, 1000);
        printAccretionDiskDiameters(calculatedBlackHoleAngularDiameterInMoonFractions, 5000);
    }

    private void printAccretionDiskDiameters(double calculatedBlackHoleAngularDiameterInMoonFractions, int size) {
        println("When accretion disk is " + size + " times the event horizon radius: " + round(calculatedBlackHoleAngularDiameterInMoonFractions * size, 6) + " of moon's angular diameter",
                size + " times bigger accretion disk: " + round(calculatedBlackHoleAngularDiameterInMoonFractions * size, 6) + " of moon's size");
    }

    private void println(String fullMessage, String compactMessage) {
        if(compactMode) {
            System.out.println(compactMessage);
        } else {
            System.out.println(fullMessage);
        }
    }

    private void println(String message) {
        println(message, message);
    }

    private void println() {
        println("");
    }
    public static void start(Map<String, String> args) {
        new StellarStabilityApp(args).run();
    }

    private Double retrieve(String key) {
        return Double.parseDouble(arguments.get(key));
    }

    private String retrieveString(String key) {
        return arguments.get(key);
    }

    private double round(double x, double b) {
        double y = Math.pow(10, b) * x;
        y = Math.round(y);
        return y / Math.pow(10, b);
    }
}
